import Executor from '../services/executor'

class Element {
  /**
   * @param {Element|WebdriverIO.Client} parent
   * @param {string|WebdriverIO.Client.<P>} selector
   * @param {string} description
   */
  constructor(parent, selector, description) {
    this._description = description

    if (parent.webDriverElement) {
      this._browser = parent.browser
      this._parent = parent
      this._waitForTimeout = this.parent.waitForTimeout

      if (typeof selector === 'string') {
        this._selector = selector
        try {
          this._load()
        } catch (timeoutError) {
          throw new Error(this.getElementFullName() + ' is not found. \n' + timeoutError.toString())
        }
      } else {
        this._selector = selector.selector
        this._webDriverElement = selector
      }
    } else {
      this._browser = parent
      this._parent = null
      this._webDriverElement = this._browser
      this._waitForTimeout = this._browser.options.waitforTimeout
    }
  }

  /**
   * @return {WebdriverIO.Client} browser
   */
  get browser() {
    return this._browser
  }

  /**
   * @return {Page|Element|null}
   */
  get parent() {
    return this._parent
  }

  /**
   * @return {string}
   */
  get description() {
    return this._description
  }

  /**
   * @return {string|null}
   */
  get selector() {
    return this._selector
  }

  /**
   * @return {Element|(WebdriverIO.Client.<string[]>&string[])|(WebdriverIO.Client.<string>&string)|WebdriverIO.Client.<P>|null|*}
   */
  get webDriverElement() {
    return this._webDriverElement
  }

  /**
   * @return {number}
   */
  get waitForTimeout() {
    return this._waitForTimeout
  }

  /**
   * Load webdriver element, load is not accessible for pages
   */
  _load() {
    if (this.parent && this.parent.webDriverElement) {
      new Executor(this._browser).waitForCondition(() => this.parent.has(this.selector), `Element is existing`, this.waitForTimeout)
      this._webDriverElement = this.parent.webDriverElement.element(this.selector)
    }
  }

  /**
   * @return {string}
   */
  getElementFullName() {
    let name = 'Element ' + this.description + '(' + this.selector + ')'
    if (this.parent) {
      name += ' [parent element: ' + this.parent.description + (this.parent.selector ? '(' + this.parent.selector + ')' : '') + ']'
    }
    return name
  }

  /**
   * Get element by selector or throw exception if it is not found
   * @param {string} selector
   * @param {string} elementDescription
   * @return {Element}
   */
  getElement(selector, elementDescription) {
    return new Element(this, selector, elementDescription)
  }

  /**
   * @param {string} selector
   * @return {boolean}
   */
  has(selector) {
    return this._webDriverElement.isExisting(selector)
  }

  /**
   * @return {boolean}
   */
  isExisting() {
    return this.parent.has(this.selector)
  }

  /**
   * @return {boolean}
   */
  isVisible() {
    return this.webDriverElement.isVisible()
  }

  /**
   * Get elements array by selector
   * @param {string} selector
   * @param {string} elementDescription
   * @param {elementConstructor} [elementConstructor]
   * @return {Element[]}
   */
  getElements(selector, elementDescription, elementConstructor = undefined) {
    const webDriverElements = this.webDriverElement.elements(selector).value
    const elements = []

    if (!elementConstructor) {
      elementConstructor = function(parent, webDriverElement, elementDescription) {
        return new Element(parent, webDriverElement, elementDescription)
      }
    }

    let index = 1
    for (const webDriverElement of webDriverElements) {
      const element = elementConstructor(this, webDriverElement, elementDescription)
      element._selector = `${selector}[${index}]`
      if (element) {
        elements.push(element)
      }
      index++
    }
    if (elements.length === 0) {
      throw new Error('Collection of elements ' + this.getElement(selector, elementDescription).getElementFullName() + ' is not found.')
    }
    return elements
  }

  /**
   * Wait for button is clickable and click on it
   *
   * @return {Element}
   */
  click() {
    new Executor(this.browser)
      .setOnFail(() => this._load())
      .retry(() => {
        try {
          this.webDriverElement.click()
        } catch (clickError) {
          throw new Error(this.getElementFullName() + ' is not clickable. \n' + clickError.toString())
        }
      })
    return this
  }

  /**
   * Type some string to the input
   * @param {string} text
   * @return {Element}
   */
  type(text) {
    this.backspaceClean()
    this.webDriverElement.setValue(text)
    return this
  }

  /**
   * Clean element using backspaces.
   * Should be used if the input has two-way binding,
   * to ensure the bound element is updated
   * @returns {Element}
   */
  backspaceClean() {
    const currentValue = this.webDriverElement.getValue()
    if (currentValue.length > 0) {
      this.webDriverElement.click()
      const backspaces = new Array(currentValue.length).fill('Backspace')
      this.webDriverElement.keys('End')
      this.webDriverElement.keys(backspaces)
    }
    return this
  }

  /**
   * Get text of element
   * @return {string}
   */
  getText() {
    return this.webDriverElement.getText()
  }

  /**
   * Return element if it exist
   * @param {string} selector
   * @param {string} description
   * @return {Element|null}
   */
  getOptional(selector, description) {
    if (this.has(selector)) {
      return this.getElement(selector, description)
    }
    return null
  }

  /**
   * Wait for element exists
   */
  waitForExist() {
    try {
      this.webDriverElement.waitForExist(this._waitForTimeout);
    } catch (timeoutError) {
      throw new Error(this.getElementFullName() + ' does not exist.\n' + timeoutError.toString());
    }
  }

  /**
   * @param {number} [customTimeout]
   * Wait for element not exists
   */
  waitForNotExist(customTimeout = this._waitForTimeout) {
    try {
      this.webDriverElement.waitForExist(customTimeout, true);
    } catch (timeoutError) {
      throw new Error(this.getElementFullName() + ' does exist.\n' + timeoutError.toString());
    }
  }
}

export default Element

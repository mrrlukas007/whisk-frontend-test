import WhiskPage from '../whisk-page'
import Executor from 'packages/services/executor'

class LoginPage extends WhiskPage {
  /**
   * @param {PageFactory} pageFactory
   */
  constructor(pageFactory) {
    super(pageFactory)
    this.loginField = this.getElement('//input[@data-testid="UI_KIT_INPUT"]', 'Login field')
    this.continueButton = this.getElement('//button[@data-testid="auth-continue-button"]', 'Continue button')
    this.errorMessageField = this.getElement('//div[@data-testid="email-phone-number-auth-input"]/span', 'Error field')
  }

  /**
   * @param {string} value
   */
  typeLogin(value) {
    this.loginField.type(value)
  }

  pressContinue() {
    this.continueButton.click()
  }

  submitForm() {
    this.pressContinue()
    new Executor(this.browser).waitForCondition(() => !this.has('//div[@data-testid="authentication-form"]'), 'Waiting absence submit form')
    this.pageFactory.openShoppingListPage()
  }

  /**
   * @return {string}
   */
  getErrorMessage() {
    return this.errorMessageField.getText()
  }
}

export default LoginPage

import Element from 'packages/pages/element'

class ShoppingListItem extends Element {
  /**
   * @param {Element} parent
   * @param {string} selector
   * @param {string} description
   */
  constructor(parent, selector, description) {
    super(parent, selector, description)
    this.nameField = this.getElement('.//span[@data-testid="shopping-list-item-name"]', 'Item name field')
    this.checkbox = this.getElement('.//button[@data-testid="shopping-list-item-checkbox"]', 'Checkbox')
  }

  /**
   * @return {string}
   */
  getName() {
    return this.nameField.getText()
  }

  pressCheckbox() {
    this.checkbox.click()
  }
}

export default ShoppingListItem

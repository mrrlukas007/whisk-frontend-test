import WhiskPage from '../whisk-page'
import ShoppingListItem from './shopping-list-item'

class ShoppingListPage extends WhiskPage {
  /**
   * @param {PageFactory} pageFactory
   */
  constructor(pageFactory) {
    super(pageFactory)
    this.shoppingListNameField = this.getElement('//div[@data-testid="shopping-list-name"]/h2 ', 'Shopping list name field')
    this.menuShoppingListNameField = this.getElement('//div[@data-testid="shopping-lists-list-name"]', 'Menu shopping list name field')
    this.optionsListButton = this.getElement('//button[@data-testid="vertical-dots-shopping-list-button"]', 'Options list button')
    this.itemsCountField = this.getElement('//div[@data-testid="shopping-lists-list-name"]/../div[2]', 'Items count field')
    this.emailField = this.getElement('//button[@data-testid="avatar-button"]/div[2]', 'Email field')
    this.addItemField = this.getElement('//input[@data-testid="desktop-add-item-autocomplete"]', 'Add item field')
    this.emptyListMessageField = this.getElement('//h1', 'Empty list message field')
  }

  /**
   * @return {string}
   */
  getEmail() {
    return this.emailField.getText()
  }

  /**
   * @return {string}
   */
  getShoppingListName() {
    return this.shoppingListNameField.getText()
  }

  /**
   * @return {string}
   */
  getMenuShoppingListName() {
    return this.menuShoppingListNameField.getText()
  }

  /**
   * @return {string}
   */
  getEmptyListMessage() {
    return this.emptyListMessageField.getText()
  }

  /**
   * @return {string}
   */
  getItemsCount() {
    return this.itemsCountField.getText()
  }

  /**
   * @param {string} name
   */
  renameShoppingList(name) {
    this.optionsListButton.click()
    this.getElement('//button[@data-testid="shopping-list-rename-menu-button"]', 'Rename button').click()
    this.getElement('//input[@data-testid="UI_KIT_INPUT"]', 'Name field').type(name)
    const saveButton = this.getElement('//button[@data-testid="create-new-shopping-list-create-button"]', 'Save field')
    saveButton.click()
    saveButton.waitForNotExist()
    this.pageFactory.openShoppingListPage()
  }

  copyShoppingList() {
    this.optionsListButton.click()
    this.getElement('//button[@data-testid="shopping-list-make-copy-menu-button"]', 'Copy list button').click()
  }

  clearShoppingList() {
    this.optionsListButton.click()
    this.getElement('//button[@data-testid="shopping-list-clear-list-menu-button"]', 'Clear list button').click()
  }

  /**
   * @param {string[]} itemNames
   */
  addItems(itemNames) {
    this.addItemField.click()
    itemNames.forEach((itemName) => {
      this.getElement(`//div[@data-testid="autocomplete-item"]/div[2][text() = "${itemName}"]`, 'Item field').click()
    })
    this.shoppingListNameField.click()
  }

  /**
   * @return {ShoppingListItem[]}
   */
  getShoppingListItems() {
    return this.getElements(
      '//div[@data-testid="shopping-list-item"]',
      'Shopping list item',
      (parent, webDriverElement, elementDescription) => new ShoppingListItem(parent, webDriverElement, elementDescription)
    )
  }

  /**
   * @return {ShoppingListItem}
   */
  getShoppingListItemByName(itemName) {
    return new ShoppingListItem(
      this,
      `//span[@data-testid="shopping-list-item-name" and text() = "${itemName}"]/ancestor::div[@data-testid="shopping-list-item"]`,
      'Shopping list item'
    )
  }

  /**
   * @param {string} listName
   */
  getShoppingListItemsCountByName(listName) {
    return this.getElement(
      `//div[@data-testid="shopping-lists-list-name" and text() = "${listName}"]/../div[2]`,
      'Shopping list items count'
    ).getText()
  }
}

export default ShoppingListPage

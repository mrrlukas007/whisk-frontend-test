import ShoppingListPage from '../shopping-list-page/shopping-list-page'
import LoginPage from '../login-page/login-page'
import WhiskPage from '../whisk-page'

class PageFactory {
  /**
   * @param {WhiskTest} whiskTest
   */
  constructor(whiskTest) {
    /**
     * @type {WhiskConfig}
     * @private
     */
    this._config = whiskTest.config

    /**
     * @type {WebdriverIO.Client}
     * @private
     */
    this._browser = whiskTest.browser

    /**
     * @type {WhiskTest}
     * @private
     */
    this._test = whiskTest
  }

  /**
   * @return {WebdriverIO.Client}
   */
  get browser() {
    return this._browser
  }

  /**
   * @return {WhiskConfig}
   */
  get config() {
    return this._config
  }

  openWhisk() {
    const whiskPage = new WhiskPage(this)
    whiskPage.open()
    this._test.loginPage = new LoginPage(this)
  }

  openShoppingListPage() {
    this._test.shoppingListPage = new ShoppingListPage(this)
  }
}

export default PageFactory

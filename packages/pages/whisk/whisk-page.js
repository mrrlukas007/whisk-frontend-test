import Page from '../page'

class WhiskPage extends Page {
  /**
   * @param {PageFactory} pageFactory
   */
  constructor(pageFactory) {
    super(pageFactory.config, pageFactory.browser)
    this._pageFactory = pageFactory
  }

  /**
   * @return {PageFactory}
   */
  get pageFactory() {
    return this._pageFactory
  }
}

export default WhiskPage

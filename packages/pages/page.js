import Element from './element'

class Page extends Element {
  /**
   * @param {TestConfig} config
   * @param {WebdriverIO.Client} browser
   */
  constructor(config, browser) {
    super(browser, 'body', 'Page')
    this._config = config
    this._baseUrl = config.getBaseUrl()
  }

  /**
   * @return {WhiskConfig}
   */
  get config() {
    return this._config
  }

  /**
   * @param {string} [path]
   */
  open(path = '') {
    this.browser.url(this._baseUrl + path)
  }
}
export default Page

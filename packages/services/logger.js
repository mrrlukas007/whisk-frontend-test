import moment from 'moment'
import path from 'path'
import fs from 'fs'
import { COLORS as colors } from 'webdriverio/build/lib/helpers/constants'
import TestConfig from 'packages/base-test/test-config'
import { config as wdioConfig } from 'wdio.conf'

class Logger {
  /**
   * @param {WebdriverIO.Client} browser
   * @param {TestConfig} [config]
   */
  constructor(browser, config) {
    if (!config) {
      config = new TestConfig(wdioConfig)
    }
    this.config = config
    this.browser = browser
    this.startTime = this.browser.options.startDate || new Date()
  }

  /**
   * @return {{assertion: number, info: number, warning: number, failed: number, error: number}}
   */
  static get logLevels() {
    return {
      assertion: 100,
      info: 200,
      warning: 300,
      failed: 400,
      error: 500
    }
  }

  /**
   * @param {string} level
   * @return {boolean}
   */
  toBeLogged(level) {
    const numericLogLevel = Logger.logLevels[level]
    if (!numericLogLevel) {
      return false
    }
    return numericLogLevel >= Logger.logLevels[this.config.getDisplayLogLevel()]
  }

  /**
   * @return {moment.Moment}
   */
  getTestTime() {
    return moment.utc(moment().diff(moment(this.startTime)))
  }

  /**
   * @param {string} message
   */
  message(message) {
    if (this.toBeLogged('info')) {
      this.browser.logger.info(`[${this.getTestTime().format('HH:mm:ss')}] ${message}`)
    }
  }

  /**
   * @param {string} message
   */
  error(message) {
    if (this.toBeLogged('error')) {
      this.browser.logger.error(`[${this.getTestTime().format('HH:mm:ss')}] ${message}`)
    }
  }

  /**
   * @param {string} message
   */
  assertion(message) {
    if (this.toBeLogged('assertion')) {
      this.browser.logger.info(`[${this.getTestTime().format('HH:mm:ss')}] ${message}`)
    }
  }

  /**
   * @param {string} color
   * @param {string} type
   * @param {string} message
   * @private
   */
  _coloredLog(color, type, message) {
    this.browser.logger.log(color + type + '\t' + colors.reset + `[${this.getTestTime().format('HH:mm:ss')}] ${message}`)
  }

  /**
   * @param {string} message
   */
  warning(message) {
    if (this.toBeLogged('warning')) {
      this._coloredLog(colors.yellow, 'WARNING', message)
    }
  }

  /**
   * @param {string} message
   */
  passed(message) {
    if (this.toBeLogged('info')) {
      this._coloredLog(colors.green, 'PASSED', message)
    }
  }

  /**
   * @param {string} message
   */
  failed(message) {
    if (this.toBeLogged('failed')) {
      this._coloredLog(colors.dkred, 'FAILED', message)
    }
  }

  /**
   * @param {string} folderName
   * @param {string} fileName
   * @param {string} text
   */
  toTmpFile(folderName, fileName, text) {
    const folder = path.join(this.config.getTmpFolder(), folderName)
    if (!fs.existsSync(folder)) {
      fs.mkdirSync(folder)
    }

    fs.writeFileSync(path.join(folder, fileName), text)
  }

  /**
   * @param {string} testName
   * @param {{head, body}} htmlResult
   */
  html(testName, htmlResult) {
    let htmlText = `<html><head>${htmlResult.head}</head><body>${htmlResult.body}</body></html>`
    htmlText = htmlText.replace(new RegExp('<script', 'g'), '<noscript')
    htmlText = htmlText.replace(new RegExp('</script>', 'g'), '</noscript>')
    this.toTmpFile('html-logs', `${this.config.getInstanceName()}-${testName}.html`, htmlText)
  }

  /**
   * @param {string} testName
   */
  screenShot(testName) {
    const fullPath = path.join(this.config.getScreenShotsFolder(), `${this.config.getInstanceName()}-${testName}.png`)
    this.browser.saveScreenshot(fullPath)
  }
}

export default Logger

import Logger from './logger'

class Executor {
  /**
   * @param {WebdriverIO.Client} browser
   */
  constructor(browser) {
    /**
     * @type {WebdriverIO.Client}
     * @private
     */
    this._browser = browser
    this._onFail = null

    this.logger = new Logger(browser)

    this._lastError = null
  }

  /**
   * @return {WebdriverIO.Client}
   */
  get browser() {
    return this._browser
  }

  /**
   * @return {Error|null}
   */
  get lastError() {
    return this._lastError
  }

  /**
   * @callback callbackOnFail
   */
  /**
   * @param {callbackOnFail} callbackOnFail
   * @return {Executor}
   */
  setOnFail(callbackOnFail) {
    this._onFail = callbackOnFail
    return this
  }

  /**
   * @callback callbackForRetry
   */
  /**
   * Performs the action several times until the execution is successful and throw exception only after N retries
   * @param {callbackForRetry} callbackForRetry
   * @param {times} [times=3]
   * @return {*}
   */
  retry(callbackForRetry, times = 3) {
    let success = false
    let tryNumber = 0
    let result
    const secondsStart = new Date().getTime()

    while (!success) {
      try {
        result = callbackForRetry()
        success = true
      } catch (error) {
        tryNumber++
        const seconds = new Date().getTime() - secondsStart
        this.logger.warning(`Retry ${tryNumber} has failed (${seconds} ms).`)

        if (this._onFail) {
          this._onFail()
        }

        if (tryNumber >= times) {
          throw error
        }
        this._browser.pause(1000)
      }
    }
    return result
  }

  /**
   * Wait for condition
   * @param {function} condition condition in callback function
   * @param {string} conditionDescription
   * @param {number|null} [timeout]
   * @param {number} [interval = 100]
   */
  waitForCondition(condition, conditionDescription, timeout = null, interval = 100) {
    if (!timeout) {
      timeout = this._browser.options.waitforTimeout
    }
    try {
      this.browser.waitUntil(condition, timeout, interval)
    } catch (timeoutError) {
      throw new Error(
        `Expected condition: "${conditionDescription}" does not satisfy actual condition after ${timeout} seconds.\n` + timeoutError.toString()
      )
    }
  }
}

export default Executor

import Logger from 'packages/services/logger'

class BaseTest {
  /**
   * @param {WebdriverIO.Client} browser
   * @param {TestConfig} config
   */
  constructor(browser, config) {
    this._browser = browser
    this._config = config
    this.logger = new Logger(browser, config)
    this.logger.message(`Test ${this.constructor.name} started`)
    this.browser.windowHandleMaximize()
  }

  /**
   * @return {TestConfig}
   */
  get config() {
    return this._config
  }

  /**
   * @return {WebdriverIO.Client}
   */
  get browser() {
    return this._browser
  }

  /**
   * @param {string} description
   * @param {callback} callback
   */
  it(description, callback) {
    it(description, () => {
      try {
        callback()
        this.logger.passed(description)
      } catch (error) {
        if (this._config.isBrowserHtmlLogged()) {
          const result = this.browser.execute(() => ({
            head: document.head.innerHTML,
            body: document.body.innerHTML
          }))
          this.logger.screenShot(this.constructor.name)
          this.logger.html(this.constructor.name, result.value)
        }
        this.logger.failed(description)
        this.logger.error(error.message)
        throw error
      }
    })
  }
}

export default BaseTest

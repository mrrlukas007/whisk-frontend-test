import path from 'path'

class TestConfig {
  /**
   * @param {{}} configFile Specific inheritor config's JSON.
   */
  constructor(configFile) {
    if (!configFile) {
      throw new Error("JSON of inheritor's config must be passed")
    }

    /**
     * @type {{}}
     */
    this.configFile = configFile
  }

  /**
   * @return {string}
   */
  getBaseUrl() {
    return this.configFile.baseUrl
  }

  /**
   * @return {string}
   */
  getInstanceName() {
    return this.configFile.instanceName
  }

  /**
   * @return {string}
   */
  getTmpFolder() {
    return path.join(__dirname, '..', '..', 'tmp')
  }

  /**
   * @return {string}
   */
  getScreenShotsFolder() {
    return path.join(__dirname, '..', '..', 'error-shots')
  }

  /**
   * @return {boolean}
   */
  isBrowserHtmlLogged() {
    return this.configFile.browserHtmlLog
  }

  /**
   * @return {string}
   */
  getDisplayLogLevel() {
    return this.configFile.displayLogLevel
  }
}

export default TestConfig

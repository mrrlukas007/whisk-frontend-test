import assert from 'assert'

class Assertion {
  /**
   * @param {BaseTest} test
   */
  constructor(test) {
    if (!test) {
      throw new Error('Test instance must be passed')
    }

    /**
     * @type {BaseTest}
     */
    this.test = test
  }

  /**
   * @param {number|string|boolean} actual
   * @param {number|string|boolean} expected
   * @param {string} message
   */
  strictEqual(actual, expected, message) {
    message = this.getMessageForEqual(actual, expected, message)
    this.logAssert(message)
    assert.strictEqual(actual, expected, message)
  }

  /**
   * @param {string} actual
   * @param {string} expected
   * @param {string} message
   */
  includes(actual, expected, message) {
    message = message + `: actual value: ${actual} contains ${expected}`
    this.logAssert(message)
    assert.ok(actual.includes(expected), message)
  }

  /**
   * @param {*} value
   * @param {string} message
   */
  ok(value, message) {
    this.logAssert(message)
    assert.ok(value, message)
  }

  /**
   * @param {number|string|boolean} actual
   * @param {number|string|boolean} expected
   * @param {string} message
   */
  getMessageForEqual(actual, expected, message) {
    return message + `: actual value ${actual} == expected value: ${expected}`
  }

  /**
   * @param {string} message
   */
  logAssert(message) {
    this.test.logger.assertion(message)
  }
}

export default Assertion

const fs = require('fs')
const path = require('path')

require('app-module-path').addPath(path.join(__dirname, '/../../..'))
const wdioConfig = require('wdio.conf').config

let whiskConfig = {
  //
  // ==================
  // Specify Test Files
  // ==================
  // Define which tests specs should run. The pattern is relative to the directory
  // from which `wdio` was called. Notice that, if you are calling `wdio` from an
  // NPM script (see https://docs.npmjs.com/cli/run-script) then the current working
  // directory is where your package.json resides, so `wdio` will be called from there.
  //
  specs: ['./specs/whisk/**/run.js'],
  suites: {
    whisk: ['./specs/whisk/**/run.js']
  },
  // Patterns to exclude.
  exclude: [],

  // Whisk base url
  baseUrl: 'https://dev.whisk.com/',
  instanceName: 'whisk'
}

whiskConfig = { ...wdioConfig, ...whiskConfig }

const envPath = path.join(__dirname, 'env.js')
// Merge environment config if exists:
if (fs.existsSync(envPath)) {
  whiskConfig = { ...whiskConfig, ...require(envPath) }
}

exports.config = whiskConfig

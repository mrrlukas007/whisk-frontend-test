import TestConfig from 'packages/base-test/test-config'
import { config } from './whisk.conf'

class WhiskConfig extends TestConfig {
  constructor() {
    super(config)
  }
}

export default WhiskConfig

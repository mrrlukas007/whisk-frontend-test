import Assertion from 'packages/assertions/assertion'
import BaseTest from 'packages/base-test/base-test'
import PageFactory from 'packages/pages/whisk/service/page-factory'

class WhiskTest extends BaseTest {
  /**
   * @param {WebdriverIO.Client} browser
   * @param {WhiskConfig} config
   */
  constructor(browser, config) {
    super(browser, config)
    /**
     * @type {LoginPage}
     */
    this.loginPage = null

    /**
     * @type {ShoppingListPage}
     */
    this.shoppingListPage = null

    /**
     * @type {PageFactory}
     * @private
     */
    this._pageFactory = new PageFactory(this)

    /**
     * @type {Assertion}
     * @private
     */
    this._assert = new Assertion(this)
  }

  /**
   * @return {WhiskConfig}
   */
  get config() {
    return this._config
  }

  /**
   * @return {PageFactory}
   */
  get pageFactory() {
    return this._pageFactory
  }

  /**
   * @return {Assertion}
   */
  get assert() {
    return this._assert
  }
}

export default WhiskTest

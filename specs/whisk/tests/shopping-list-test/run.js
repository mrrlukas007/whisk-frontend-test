import ShoppingListTest from './shopping-list-test'
import WhiskConfig from 'specs/whisk/config/whisk-config'
const config = new WhiskConfig()

const test = new ShoppingListTest(browser, config)
test.shoppingList()

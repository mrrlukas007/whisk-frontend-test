import WhiskTest from '../../whisk-test'

const timeStamp = new Date().getTime()
const correctEmail = `test_email${timeStamp}@gmail.com`
const incorrectEmail = `incorrect_email${timeStamp}.com`
const defaultShoppingListName = 'Shopping List'
const newShoppingListName = 'Awesome shopping list'
const listItems = ['Milk', 'Bread', 'Carrots', 'Eggs']

class ShoppingListTest extends WhiskTest {
  shoppingList() {
    describe('Whisk shopping list test', () => {
      this.it('Try to login to Whisk with incorrect data', () => {
        this.pageFactory.openWhisk()
        this.loginPage.pressContinue()
        this.assert.strictEqual(this.loginPage.getErrorMessage(), 'This field is required', 'Check empty email error message')
        this.loginPage.typeLogin(incorrectEmail)
        this.loginPage.pressContinue()
        this.assert.strictEqual(
          this.loginPage.getErrorMessage(),
          'Make sure your email or phone number is right',
          'Check incorrect email error message'
        )
      })
      this.it('Login to Whisk', () => {
        this.loginPage.typeLogin(correctEmail)
        this.loginPage.submitForm()
      })
      this.it('Check shopping list default information', () => {
        this.assert.strictEqual(this.shoppingListPage.getEmail(), correctEmail, 'Check user email')
        this.assert.strictEqual(this.shoppingListPage.getEmptyListMessage(), 'Add something to your list', 'Check empty list message')
        this.assert.strictEqual(this.shoppingListPage.getShoppingListName(), defaultShoppingListName, 'Check default shopping list name')
        this.assert.strictEqual(
          this.shoppingListPage.getMenuShoppingListName(),
          defaultShoppingListName,
          'Check default shopping list name in shopping list menu'
        )
        this.assert.strictEqual(this.shoppingListPage.getItemsCount(), '0 items', 'Check empty count list')
      })
      this.it('Rename shopping list', () => {
        this.shoppingListPage.renameShoppingList(newShoppingListName)
        this.assert.strictEqual(this.shoppingListPage.getShoppingListName(), newShoppingListName, 'Check renamed shopping list name')
        this.assert.strictEqual(
          this.shoppingListPage.getMenuShoppingListName(),
          newShoppingListName,
          'Check renamed shopping list name in shopping list menu'
        )
      })
      this.it('Add items to shopping list', () => {
        this.shoppingListPage.addItems(listItems)
        this.assert.strictEqual(this.shoppingListPage.getItemsCount(), `${listItems.length} items`, 'Check new added list count')
        const actualItems = this.shoppingListPage.getShoppingListItems()
        this.assert.strictEqual(actualItems.length, listItems.length, 'Check items count in list')
        actualItems.forEach((item) => {
          const actualItemName = item.getName()
          this.assert.ok(listItems.includes(actualItemName), `Check item name ${actualItemName}`)
        })
      })
      this.it('Copy shopping list', () => {
        this.shoppingListPage.copyShoppingList()
        this.assert.strictEqual(
          this.shoppingListPage.getShoppingListItemsCountByName(`Copy of ${newShoppingListName}`),
          `${listItems.length} items`,
          'Check copied list items count'
        )
      })
      this.it('Check one item and clear the list', () => {
        const itemToCheck = listItems.pop()
        this.shoppingListPage.getShoppingListItemByName(itemToCheck).pressCheckbox()
        this.assert.strictEqual(this.shoppingListPage.getItemsCount(), `${listItems.length} items`, 'Check list count for remaining items')
        this.shoppingListPage.clearShoppingList()
        this.assert.strictEqual(this.shoppingListPage.getItemsCount(), '0 items', 'Check empty list count')
        this.assert.strictEqual(this.shoppingListPage.getEmptyListMessage(), 'Add something to your list', 'Check empty list message')
      })
    })
  }
}

export default ShoppingListTest

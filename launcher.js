const { Launcher } = require('webdriverio')

new Launcher(process.argv[2], { startDate: new Date() }).run().then(
  (code) => process.exit(code),
  (error) => {
    console.error('Launcher failed to start the test', error /* .stacktrace */)
    process.exit(1)
  }
)

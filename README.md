# Whisk interview test

This repo contains selenium tests for Whisk platform.
Now tests are checking https://whisk.

## 1.  Overview
Selenium tests are based on [Webdriver.io](https://www.npmjs.com/package/webdriverio) and are written on JavaScript. Full documentation can be found [here](http://webdriver.io/api.html). 

* [Mocha.Js](https://mochajs.org/) framework is used for describing test-suites and assertions.
* Tests are executed in Chrome browser by default.

## 2. Requirements
- NodeJs v10.18.1
- Selenium server
- NPM
- Java

## 3. Installation
- do git clone to your folder
- install dependencies
```
npm install
```
- install selenium standalone server
```
npm install selenium-standalone@latest -g
selenium-standalone install
```
- windows specific dependencies
```
npm install --global --production windows-build-tools
npm install --global node-gyp
```

## 4. Running
- Start selenium server:
```
selenium-standalone start
or
npm run selenium
```

- Run specific suit:
```
npm run whisk
```

- Run specific test:
```
npm run whisk-test specs/whisk/tests/shopping-list-test/run.js
```

## 5. Check allure report

- Generate allure report:
```
allure generate allure-results/
```

- Open allure report:
```
allure open
```
